#!/usr/bin/python

import numpy as np
import sympy as sp
from IPython.display import display
import math
import time
import sys
import matplotlib.pyplot as plt
from servos import ServoSB
from links import MultiLink

class RobotArm(object):
  def __init__(self):
    # Connect the servos to their pin and select the range of the angles
    self.servos = {"base": ServoSB(11, -90, 90),
                   "shoulder": ServoSB(15, -90, 90),
                   "elbow": ServoSB(13, -90, 90),
                   "grip": ServoSB(17, 0, 90)}

    # TODO: Create the body of the robot using DH conventions
    self._joints = {_s("j1"): 0.0,
                    _s("j2"): 0.0,
                    _s("j3"): 0.0,
                    _s("j4"): 0.0}
    
    self._body = MultiLink()

    _dh = {"d": [6.5, .0, .0, .0], #sobre z
           "theta": ["s:j1", "s:j2  - 90.0", "s:j3 + 90.0", "s:j4"], #sobre z
           "r": [.0, 9.1, 12., 9.4], #sobre x
           "alpha": [-90.0, 0.0, .0, .0] #sobre x
           }

    self._body.fromDH(_dh)
    self._body.compose(*self._joints.keys())
    self._J = self._body.J.copy()
    print self._body.tt  

  def moveLink(self, link, angle):
    #Move the appropriate servo to the desired angle
    print("Moving %s to %d"%(link, angle))  
    if link in self.servos.keys():
      servos[link].set_angle(angle)

  def _setJoints(self, angles):
    self._joints = {_s("j1"): float(str(angles[0]).strip()),
                    _s("j2"): float(str(angles[1]).strip()),
                    _s("j3"): float(str(angles[2]).strip()),
                    _s("j4"): float(str(angles[3]).strip())}    
                    
    self._body.bindSymbols(self._joints)                   
  
  def readJoints(self):
  
    Q = sp.Matrix( [self._joints[_s("j1")],
                    self._joints[_s("j2")], 
                    self._joints[_s("j3")],
                    self._joints[_s("j4")]
                   ])
                    

    return Q
  
  def getJacobian(self):
    self.readJoints()
    return self._body.getJacobian()               

  def gotoPoint(self, tx, ty, tz, steps=1):
    """ Simple control loop that uses the Jacobian"""
    print "Moving to:",(tx,ty,tz)
    for i in range (steps):
      print "****** Iteration #%d:"%i
      # Read current joint status
      Q = self.readJoints()
      display(Q)
      
      # Draw current robot position
      self._body.plotLinks()
      plt.savefig("body_%d.png"%i)
      
      # Get position in 3D space (DK)
      pose = self._body.jointsToPosition(Q)
      print "Pose:"
      display(pose)
      # Get distance from target
      E = sp.Matrix([[tx - pose[0]],
                     [ty - pose[1]],
                     [tz - pose[2]],
                     [1]])
      display(E)
      print "Es:",E.shape
      
      #Compute jacobian
      J = self.getJacobian()
      display(J)
      print "Js:",J.shape
      
      # Calculate joint change
      dQ =  J.T * E*0.005
      display(dQ)
      # and use it to update current values
      Q +=  dQ
      self.moveJoints(Q)

  def gotoPointIK(self, x, y, z, ik_pos_threshold=.1, max_tries=1, damp_deg=360, max_sleep=15):
      current_tries = 0
      current_effector_index = len(self._body.links)
      servos = ['base', 'shoulder', 'elbow', 'grip']
      symbols = [_s("j1"), _s("j2"), _s("j3"), _s("j4")]
      _minAngles = [self.servos[s].minAngle for s in servos]
      _maxAngles = [self.servos[s].maxAngle for s in servos]                  

      while current_tries < max_tries:
        print("Iteration",current_tries)
        print("Effector",current_effector_index)
        current_z_rotation = self.readJoints()
        print("Original Q:",current_z_rotation)

        # Extract the x, y and z coordinates of the current effector root from Hs
        # Corresponds to R in Figure 3A
        eff_root_pos = sp.Matrix(
            (self._body.tt.Hs[current_effector_index - 1].subs(self._joints)*sp.Matrix([0,0,0,1])
            )[:-1])
        # Extract the x, y and z coordinates of the current effector end from Hs
        # Corresponds to E in Figure 3A
        eff_end_pos = sp.Matrix(
            (self._body.tt.Hs[-1].subs(self._joints)*sp.Matrix([0,0,0,1])
            )[:-1])

        desired_end = sp.Matrix([x, y, z])
                
        # Calculate the squared distance between the current effector end and the desired position
        _curr_dist = (eff_end_pos - desired_end).norm()
        if (_curr_dist) <= ik_pos_threshold:
          print("Reached threshold")
          break

        # Construct the current vector (corresponding to RE in Figure 3A)
        current_vector = eff_end_pos - eff_root_pos
        print("Current vector",current_vector)
        
        # Construct the target vector (corresponding to RD in Figure 3A)
        target_vector = desired_end - eff_root_pos
        print("Target vector",target_vector)                

        ic_O = sp.Matrix([1,0,0])
        jc_O = sp.Matrix([0,1,0])        
        p_target_vector = sp.Matrix([target_vector.dot(ic_O) / ic_O.norm(), target_vector.dot(jc_O) / jc_O.norm(), 0])
        print("Target vector projected to the rot plane", p_target_vector)                                
        # Calculate the cos of the angle between the 2 vectors
        cos_angle = angle_between(p_target_vector, current_vector)
        turn_deg = math.degrees(cos_angle)
        
        servo_max_angle = self.servos[servos[current_effector_index -1]].maxAngle
        servo_min_angle = self.servos[servos[current_effector_index -1]].minAngle


        print("Rotation requested:", turn_deg)  
        
        if turn_deg < servo_min_angle:
          turn_deg = servo_min_angle
                    
       
        if turn_deg > servo_max_angle:
          turn_deg = servo_max_angle
        
        print("Rotation after clipping:", turn_deg)                
        current_z_rotation[current_effector_index -1] = turn_deg
        print("Rotation to send:", current_z_rotation)                        


        # Update the index of the current link
        if current_effector_index > 1:
            current_effector_index -= 1
        else:
            current_effector_index = len(self._body.links) 

        # Increment the number of tries
        current_tries += 1      
        #current_z_rotation = [current_bindings[symbols[0]], current_bindings["j2"], current_bindings["j3"], current_bindings["j4"]]
        self.moveJoints(current_z_rotation)
        line = None
        while(line is None):
          try:
              line = self.arduino.readline()
          except serial.serialutil.SerialException:
              print("Error reading line!")
        print("Exited ", current_tries-1, " iteration. Starting the ", current_tries, " iteration")
        time.sleep(max_sleep)
        print('---------------------------------------------------------------------')        


      
if __name__ == '__main__':
  
  arm = RobotArm() 
  # TODO: connect with the gamepad




