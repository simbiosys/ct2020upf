import time
import cv2
import cv2.aruco as aruco
import numpy as np
import argparse
import traceback
import sys
import pickle 

def open_camara(cid):
  cap = cv2.VideoCapture(cid)
  return cap

def init_aruco():
  dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_1000)
  board = cv2.aruco.CharucoBoard_create(8,5,.025,.0125,dictionary)
  img = board.draw((200*3,200*3))

  #Dump the calibration board to a file
  cv2.imwrite('charuco_board.png',img)

  return board, dictionary
  
def doCalib(args):
  cam = args.cam
  frames = args.n

  board, dictionary = init_aruco()

  cap = open_camara(cam)

  print("Please adjust board position to ensure that it fits completely in camera")
  print("Strike 'c' to start calibration")
  while(True):
    ret, frame = cap.read()		
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) == ord('c'):
      break

  print("Starting frames acquisition...")
  allCorners = []
  allIds = []
  while(len(allCorners) < frames):
    print("Frame %d of %d"%(len(allCorners), frames))  
    ret, frame = cap.read()
    res = cv2.aruco.detectMarkers(frame, dictionary)

    if len(res[0])>0:
      res2 = cv2.aruco.interpolateCornersCharuco(res[0],res[1],frame, board)
      cv2.aruco.drawDetectedMarkers(frame,res[0],res[1])      
      cv2.imshow('frame',frame)
      if cv2.waitKey(1) == ord('c'):
        if (res2[1] is not None) and (res2[2] is not None) and (len(res2[1])>3):
          allCorners.append(res2[1])
          allIds.append(res2[2])

    if cv2.waitKey(1) == ord('q'):
      print("Stopped after acquiring %d frames"%(len(allCorners)))
      break

  cap.release()
  cv2.destroyAllWindows()

  print("Performing calibration with %d frames..."%len(allCorners))
  rett,mtx,dist,rvecs,tvecs = (None, None, None, None, None)
    
  # Extract 1 channel shape
  imsize = frame.shape[::1][1:3]

  #Calibration fails for lots of reasons. Release the video if we do
  try:
    rett,mtx,dist,rvecs,tvecs = cv2.aruco.calibrateCameraCharuco(allCorners, allIds,board,imsize,None,None)
    if rett: # If calibration was successfull return value is True:
      with open(args.calib, "wb") as fp:
        pickle.dump((mtx,dist,rvecs,tvecs), fp)
  except:
    cap.release()
    print(sys.exc_info()) #traceback.print_exc()    

  print(rett,mtx,dist,rvecs,tvecs)  
  print("...done")
  

def doPlay(args):
  # TODO: load frames from imgDir
  
  # TODO: perform pivot calibration
  pass
  
def doRecord(args):
  # Load calibration
  with open(args.calib, "rb") as fp:
    camera_matrix, dist_coeffs, rvecs, tvecs = pickle.load(fp)

  w,h = (1920, 1080)
  new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coeffs, (w,h), 1, (w,h))  
  
  map1, map2 = cv2.initUndistortRectifyMap(camera_matrix, dist_coeffs, None, new_camera_matrix, (w,h), cv2.CV_16SC2)

  cap = open_camara(args.cam)
  aruco_dict = aruco.Dictionary_get( aruco.DICT_6X6_1000 )
  markerLength = 20   # Here, our measurement unit is centimetre.
  arucoParams = aruco.DetectorParameters_create()

  # TODO: save data in imgDir
  imgDir = "imgSequence"  # Specify the image directory

  while(True):
    ret, img = cap.read()  
    imgRemapped = cv2.remap(img, map1, map2, cv2.INTER_LINEAR) # for linear remapping
    imgRemapped_gray = cv2.cvtColor(imgRemapped, cv2.COLOR_BGR2GRAY)    # aruco.detectMarkers() requires gray image
    corners, ids, rejectedImgPoints = aruco.detectMarkers(imgRemapped_gray, aruco_dict, parameters=arucoParams) # Detect aruco
    if type(ids) != type(None): # if aruco marker detected
      print("Detected marker: ",ids[:,0].tolist())    
      rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners, markerLength, camera_matrix, dist_coeffs) # posture estimation from a single marker
      imgWithAruco = aruco.drawDetectedMarkers(imgRemapped, corners, ids, (0,255,0))
      for mn, mid in enumerate(ids[:,0].tolist()):
        _r = rvec[mn,:,:]; _t = tvec[mn,:,:];
        rotMat,_ = cv2.Rodrigues(_r)        
        imgWithAruco = aruco.drawAxis(imgWithAruco, camera_matrix, dist_coeffs, _r, _t, 50)    # axis length 100 can be changed according to your requirement      
    else:   # if aruco marker is NOT detected
      imgWithAruco = imgRemapped  # assign imRemapped_color to imgWithAruco directly

    cv2.imshow("aruco", imgWithAruco)   # display

    if cv2.waitKey(1) & 0xFF == ord('q'):   # if 'q' is pressed, quit.
      break  
      
  cap.release()
  cv2.destroyAllWindows()      
 
  
if __name__ == "__main__":

  parser = argparse.ArgumentParser()
  parser.add_argument('--cam', help ="choose a camera", type = int, default = 0)
  parser.add_argument('--calib', help ="Calibration file", type = str,
    default="camera_calibration.pickle")      
  
  subp = parser.add_subparsers(title="subcommands")
  calib = subp.add_parser("calibrate")
  calib.add_argument('-n', help ="Number of frames", type = int, default = 10)
  calib.set_defaults(func=doCalib)
  
  record = subp.add_parser("record")
  record.set_defaults(func=doRecord)

  play = subp.add_parser("play")
  play.set_defaults(func=doPlay)  
  
  args = parser.parse_args()  
  args.func(args)




