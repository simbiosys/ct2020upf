
import os, sys
from PyQt5 import QtCore
import cv2
import cv2.aruco
import pickle
import time
import vtk

class CVWorkerThread(QtCore.QThread):
    def __init__(self, vtk_widget, camera_calib='camera_calibration.pickle', camid=0, scale=1.0):
        super(QtCore.QThread, self).__init__()
        self.cap = cv2.VideoCapture(camid)
        self._vtk_widget = vtk_widget
        self._calibfile = camera_calib
        self._active = False
        self._scale = scale
        
    def __del__(self):
        self.wait()
        if self.cap:
          self.cap.release()

    def stop(self):
        self._active = False    
        self.terminate()
        
    def run(self):
        self._active = True
        try: 
            #TODO: Load the calibration in data
            data = None
            
        except IOError:
            print("Camera calibration file %s not found. Please run camera calibration first!"%self._calibfile)
            return
            
        print ('Starting marker detection...')

        dOptions = {
          "length": 0, # TODO: Adjust for marker size
          "calib": data,
          "dict": None, # TODO: Select the appropriate dictionary
          "param": None, # TODO: Create appropriate parameters
          "scale": self._scale
        }
        
        while(self._active):
          _t, _R = doDetectMarkerPose(self.cap, dOptions, verbose =True)
          if (_t is not None):
            transform = vtk.vtkTransform()          
            transform.Translate(_t)

            transform.Concatenate(_R)
            self._vtk_widget._tpd.SetTransform(transform)
            self._vtk_widget._tpd.Modified()
            self._vtk_widget.interactor.Render()
            time.sleep(0.01)

        cap.release()
                
def doDetectMarkerPose(cap, options, verbose=False):

  rvec, tvec, rot = None, None, None

  if tvec is not None:
      tvec = tvec.flatten()*scale
      rvec = rvec.flatten()
      rotmat, _ = cv2.Rodrigues(rvec)
      if verbose:       
        print("Tracking(%d): Translation: %s, Rotation: %s" %(
              ids, str(tvec), str(rotmat))
            )                

      rot = vtk.vtkMatrix4x4()
      for i in range(3):
        for j in range(3):
           rot.SetElement(i, j, rotmat[i, j])                
            
  return tvec, rot
                
