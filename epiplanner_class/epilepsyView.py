# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'epilepsyView.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1142, 689)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.splitter = QtGui.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.frame = QtGui.QFrame(self.splitter)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.verticalLayout = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.lstLandmarks = QtGui.QListView(self.frame)
        self.lstLandmarks.setObjectName(_fromUtf8("lstLandmarks"))
        self.verticalLayout.addWidget(self.lstLandmarks)
        self.btStartTracking = QtGui.QPushButton(self.frame)
        self.btStartTracking.setObjectName(_fromUtf8("btStartTracking"))
        self.verticalLayout.addWidget(self.btStartTracking)
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.lstFiducials = QtGui.QListView(self.frame)
        self.lstFiducials.setObjectName(_fromUtf8("lstFiducials"))
        self.verticalLayout.addWidget(self.lstFiducials)
        self.btRegistration = QtGui.QPushButton(self.frame)
        self.btRegistration.setObjectName(_fromUtf8("btRegistration"))
        self.verticalLayout.addWidget(self.btRegistration)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.xCoord = QtGui.QLineEdit(self.frame)
        self.xCoord.setObjectName(_fromUtf8("xCoord"))
        self.horizontalLayout_2.addWidget(self.xCoord)
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_2.addWidget(self.label_4)
        self.yCoord = QtGui.QLineEdit(self.frame)
        self.yCoord.setObjectName(_fromUtf8("yCoord"))
        self.horizontalLayout_2.addWidget(self.yCoord)
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_2.addWidget(self.label_5)
        self.zCoord = QtGui.QLineEdit(self.frame)
        self.zCoord.setObjectName(_fromUtf8("zCoord"))
        self.horizontalLayout_2.addWidget(self.zCoord)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.btMoveRobot = QtGui.QPushButton(self.frame)
        self.btMoveRobot.setObjectName(_fromUtf8("btMoveRobot"))
        self.verticalLayout.addWidget(self.btMoveRobot)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.vtk_panel = QtGui.QFrame(self.splitter)
        self.vtk_panel.setFrameShape(QtGui.QFrame.StyledPanel)
        self.vtk_panel.setFrameShadow(QtGui.QFrame.Raised)
        self.vtk_panel.setObjectName(_fromUtf8("vtk_panel"))
        self.horizontalLayout.addWidget(self.splitter)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1142, 28))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "EpilepsyPlanner", None))
        self.label.setText(_translate("MainWindow", "Landmarks", None))
        self.btStartTracking.setText(_translate("MainWindow", "Start Tracking", None))
        self.label_2.setText(_translate("MainWindow", "Fiducials", None))
        self.btRegistration.setText(_translate("MainWindow", "Do registration", None))
        self.label_3.setText(_translate("MainWindow", "x: ", None))
        self.label_4.setText(_translate("MainWindow", "y: ", None))
        self.label_5.setText(_translate("MainWindow", "z: ", None))
        self.btMoveRobot.setText(_translate("MainWindow", "Move robot to this point", None))

