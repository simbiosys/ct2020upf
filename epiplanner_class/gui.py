import os
from PyQt5 import uic, QtGui, QtCore, QtWidgets

from random import randrange
from time import sleep
from math import cos, sin
import numpy as np
from transforms3d.quaternions import quat2mat
from qtimpl import VTKViewer
from arucoTracker import CVWorkerThread
import vtk

def recompileui(uifname, pyfname):
    with open(uifname) as ui_file:
        with open(pyfname, "w") as py_ui_file:
            uic.compileUi(ui_file,py_ui_file)

class EpilepsyPlannerApp(QtWidgets.QMainWindow):
    def __init__(self):
        #Parent constructor
        super(EpilepsyPlannerApp,self).__init__()
        self.vtk_widget = None
        self.ui = None
        self._sub = None
        self._t = {}
        self.timer = None
        self._robot = None
        self.setup()

    def setup(self):
        import ui_epilepsyView    
        self.ui = ui_epilepsyView.Ui_MainWindow()
        self.ui.setupUi(self)
        self.vtk_widget = VTKViewer(self.ui.vtk_panel)
        self.ui.vtk_layout = QtWidgets.QHBoxLayout()
        self.ui.vtk_layout.addWidget(self.vtk_widget)
        self.ui.vtk_layout.setContentsMargins(0,0,0,0)
        self.ui.vtk_panel.setLayout(self.ui.vtk_layout)
        
        # Signal / slots
        self.ui.btStartTracking.clicked.connect(self.on_clickStartTracking)
        self.ui.btStopTracking.clicked.connect(self.on_clickStopTracking)
        self.ui.btRegistration.clicked.connect(self.on_clickRegist)
        
        self.ui.owiRadio.clicked.connect(self.on_selectedOWI)
        self.ui.meArmRadio.clicked.connect(self.on_selectedMEARM)        
        self.ui.moveoRadio.clicked.connect(self.on_selectedMOVEO)        
        
        self.ui.btRobotConnect.clicked.connect(self.on_clickRobotConnect)
        
        self.ui.j1Slider.valueChanged.connect(self.on_sj1vc)
        self.ui.j2Slider.valueChanged.connect(self.on_sj2vc)
        self.ui.j3Slider.valueChanged.connect(self.on_sj3vc)
        self.ui.j4Slider.valueChanged.connect(self.on_sj4vc)                        
        
        self.ui.btReadJoints.clicked.connect(self.on_clickReadJoints)
        self.ui.btMoveJoints.clicked.connect(self.on_clickMoveJoints) 
        
        self.ui.btDKRecalculate.clicked.connect(self.on_clickedDKRecal)               
        self.ui.btMoveRobot.clicked.connect(self.on_clickMoveIK)
        
        self.ui.btCenterCamera.clicked.connect(self.on_clickCenterCamera)
        self.ui.btUpdateScene.clicked.connect(self.on_clickUpdateScene)        
        
        self.scene_model = QtGui.QStandardItemModel(self.ui.listScene)
        self.ui.listScene.setModel(self.scene_model)
        self.vtk_widget.vfmodel = QtGui.QStandardItemModel(self.ui.Vfiducials)
        self.ui.Vfiducials.setModel(self.vtk_widget.vfmodel)
        self.vtk_widget.rfmodel = QtGui.QStandardItemModel(self.ui.Rfiducials)
        self.ui.Rfiducials.setModel(self.vtk_widget.rfmodel)        
        

    def initialize(self):
        self.vtk_widget.start()

    def on_clickUpdateScene(self):
      self.scene_model.clear()
      props = self.vtk_widget.renderer.GetViewProps()    
      for _pid in range(props.GetNumberOfItems()):
        p = props.GetItemAsObject(_pid)
        self.scene_model.appendRow([QtGui.QStandardItem(str(_pid))])            

    def on_clickCenterCamera(self):
      _actors = self.vtk_widget._actors.items()
      print("Name", "minX", "maxX", "minY", "maxY", "minZ", "maxZ")    
      mx, Mx, my, My, mz, Mz = _actors[0][1].GetMapper().GetBounds()
      
      for _k, _v in _actors:
        minX, maxX, minY, maxY, minZ, maxZ = _v.GetMapper().GetBounds()
        print(_k, minX, maxX, minY, maxY, minZ, maxZ)
        mx = min(mx, minX); my = min(my, minY); mz = min(mz, minZ)
        Mx = max(Mx, maxX); My = min(My, maxY); Mz = min(Mz, maxZ)
                
      print(mx, Mx, my, My, mz, Mz)
      self.vtk_widget.renderer.ResetCamera(mx, Mx, my, My, mz, Mz)
      self.vtk_widget.interactor.Render()      

    def on_refresh_vtk(self):
      print(self._t.keys())
      if self._t:
        if "StylusToReference" in self._t:
          _t = self._t["StylusToReference"]
          self.vtk_widget._tpd.SetTransform(_t)
          self.vtk_widget._tpd.Modified()
          self.vtk_widget.interactor.Render()
         

    @QtCore.pyqtSlot()
    def on_clickStartTracking(self):
      _interval = 100
      #self.vtk_widget._actors["skull"].SetVisibility(False)
      print('Preparing tracking thread...')
      self._thread = CVWorkerThread(self.vtk_widget, scale=20.0)
      self._thread.start()
      
      self.timer = QtCore.QTimer()
      self.timer.timeout.connect(self.on_refresh_vtk)
      self.timer.start(_interval)        
      print("Created timer to refresh vtk component every %d milliseconds: "%_interval)      

    @QtCore.pyqtSlot() 
    def on_clickStopTracking(self):
      if self.timer is not None:
        print("Stopping timer")
        self.timer.stop()
        self.timer = None
      if self._thread is not None:
          if self._thread.isRunning():
              print('Stopping thread')
              self._thread.stop()

    @QtCore.pyqtSlot() 
    def on_clickRegist(self):
      print('Start registration')
      transform = vtk.vtkTransform() # TODO: Properly define the affine transform to use
      self.vtk_widget._tpd_stl.SetTransform(transform)
      self.vtk_widget._tpd_stl.Modified()
      self.vtk_widget.interactor.Render()    
    
    @QtCore.pyqtSlot()
    def on_clickedDKRecal(self):
      Q = [
        self.ui.j1Slider.value(),
        self.ui.j2Slider.value(), 
        self.ui.j3Slider.value(),
        self.ui.j4Slider.value()    
      ]
      print("Not implemented")

    @QtCore.pyqtSlot()
    def on_clickRobotConnect(self):
      self._robot.connect()
      
    @QtCore.pyqtSlot()
    def on_sj1vc(self):
      val = self.ui.j1Slider.value()
      self.ui.j1Val.setText(str(val))
      
    @QtCore.pyqtSlot()
    def on_sj2vc(self):
      val = self.ui.j2Slider.value()
      self.ui.j2Val.setText(str(val))      
      
    @QtCore.pyqtSlot()
    def on_sj3vc(self):
      val = self.ui.j3Slider.value()
      self.ui.j3Val.setText(str(val))      
      
    @QtCore.pyqtSlot()
    def on_sj4vc(self):
      val = self.ui.j4Slider.value()
      self.ui.j4Val.setText(str(val))                              

    @QtCore.pyqtSlot()
    def on_selectedOWI(self):
      print("from simbiorob import OWI535")
      
    @QtCore.pyqtSlot()
    def on_selectedMEARM(self):
      print("from simbiorob import MeArm")

    @QtCore.pyqtSlot()
    def on_selectedMOVEO(self):
      print("from simbiorob import Moveo")

    @QtCore.pyqtSlot()
    def on_clickReadJoints(self):
      print("Not implemented")
      
    @QtCore.pyqtSlot()
    def on_clickMoveJoints(self):
      print("Not implemented")
      
    @QtCore.pyqtSlot()
    def on_clickMoveIK(self):
      print("Not implemented")    
                


