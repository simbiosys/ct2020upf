from __future__ import print_function
#import os
#from IPython.display import display
#from vtk.util.colors import tomato


#import time
#from time import sleep
#import numpy as np

#import argparse
#import traceback
#import sys
#import pickle
#import pprint
#import yaml
#from subprocess import call

import sys
from PyQt5 import QtWidgets
from gui import recompileui
from gui import EpilepsyPlannerApp


if __name__ == "__main__":
    # Recompile ui
    recompileui("epilepsyView_test.ui", "ui_epilepsyView.py")
    
    app = QtWidgets.QApplication(sys.argv)
    main_window = EpilepsyPlannerApp()
    main_window.show()
    main_window.initialize()
    app.exec_()
