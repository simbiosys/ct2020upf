
import os, sys
from PyQt5 import QtWidgets, QtGui
from QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import vtk

class VTKViewer(QtWidgets.QFrame):

    def __init__(self, parent):
        super(VTKViewer,self).__init__(parent)
        
        self.interactor = QVTKRenderWindowInteractor(self)
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.addWidget(self.interactor)
        self.layout.setContentsMargins(0,0,0,0)
        self.setLayout(self.layout)
        self._actors = {}
        self._prepare_data()
        self._pickw = "virtual"

    def _prepare_data(self):
        basedir = "data"
        filename = os.path.join(basedir, "skull2.stl")
 
        reader = vtk.vtkSTLReader()
        reader.SetFileName(filename)
        matrix = vtk.vtkMatrix4x4()
        transform = vtk.vtkTransform()
        transform.Translate([0,0,0])
        transform.Concatenate(matrix)
        tpd_stl = vtk.vtkTransformPolyDataFilter()
        tpd_stl.SetTransform(transform)
        self._tpd_stl = tpd_stl
        if vtk.VTK_MAJOR_VERSION <= 5:
            tpd_stl.SetInput(reader.GetOutput())
        else:
            tpd_stl.SetInputConnection(reader.GetOutputPort())        
         
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(tpd_stl.GetOutput())
        else:
            mapper.SetInputConnection(tpd_stl.GetOutputPort())
         
        stl_actor = vtk.vtkActor()
        stl_actor.SetMapper(mapper)
        
        # Setup VTK environment
        renderer = vtk.vtkRenderer()
        self.renderer = renderer
        self.render_window = self.interactor.GetRenderWindow()
        self.render_window.AddRenderer(renderer)

        self.interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
        self.render_window.SetInteractor(self.interactor)
        renderer.SetBackground(0.2,0.2,0.2)

        renderer.AddActor(stl_actor)
        renderer.ResetCamera()
        self._actors["skull"] = stl_actor        

        cube = vtk.vtkCubeSource()
        cube.SetXLength(0.005)
        cube.SetYLength(2)
        cube.SetZLength(2)
        cube.SetCenter(0,0,0)
        
        mapper1 = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper1.SetInput(cube.GetOutput())
        else:
            mapper1.SetInputConnection(cube.GetOutputPort())
            
        actorC = vtk.vtkActor()
        actorC.SetMapper(mapper1)
        actorC.GetProperty().SetColor(0, 0.2, 1)
        actorC.GetProperty().SetOpacity(0.2)
        
        #self.renderer.AddActor(actorC); self._actors["Cube"] = actorC        
        
        # Register pick listener

        self.b0 = stl_actor
        self.renderer = renderer
        self.picker = vtk.vtkCellPicker()
        self.picker.AddObserver("EndPickEvent", self.process_pick)
        self.interactor.SetPicker(self.picker)
        
        source_t = vtk.vtkConeSource()
        source_t.SetResolution(60)
        source_t.SetCenter(0, 0, 0)
        matrix = vtk.vtkMatrix4x4()
        transform = vtk.vtkTransform()
        transform.Translate([0,0,0])
        transform.Concatenate(matrix)
        tpd = vtk.vtkTransformPolyDataFilter()
        tpd.SetTransform(transform)
        self._tpd = tpd
        if vtk.VTK_MAJOR_VERSION <= 5:
            tpd.SetInput(source_t.GetOutput())
        else:
            tpd.SetInputConnection(source_t.GetOutputPort())
        # mapper
        mapper_t = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper_t.SetInput(tpd.GetOutput())
        else:
            mapper_t.SetInputConnection(tpd.GetOutputPort())
        
        # actor
        actor_t = vtk.vtkActor()
        actor_t.GetProperty().SetColor(0,1,0) # RGB
        actor_t.SetScale([.1,.1,.1])
        actor_t.SetMapper(mapper_t)
        self.renderer.AddActor(actor_t); self._actors["Cone"] = actor_t               

    def start(self):
        self.interactor.Initialize()
        self.interactor.Start()
        self.interactor.AddObserver(vtk.vtkCommand.KeyPressEvent, self.process_key)
        
    
    def process_key(self, obj, event):
        key = obj.GetKeyCode()
        if key == "v":
          self._pickw = "virtual"        
        elif key == "l":
          self._pickw = "real"        
    
    def _getPickedAsSphere(self, pos, color=(1,0,0)):
        # create source
        source = vtk.vtkSphereSource()
        source.SetCenter(pos[0],pos[1],pos[2])
        source.SetRadius(0.01)
         
        # mapper
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(source.GetOutput())
        else:
            mapper.SetInputConnection(source.GetOutputPort())
         
        # actor
        actor = vtk.vtkActor()
        actor.GetProperty().SetColor(*color) #RGB
        actor.SetMapper(mapper)      
        return actor
    
    def process_pick(self, obj, event):
        point_id = obj.GetPointId()
        if point_id >= 0:
            pos = obj.GetPickPosition()
            print(pos)
            item = QtGui.QStandardItem(str(pos))
            if self._pickw == "virtual":
              self.vfmodel.appendRow(item)
              col = (1,0,0)
            if self._pickw == "real":
              self.rfmodel.appendRow(item)              
              col = (0,0,1)              
            actor = self._getPickedAsSphere(pos, col)

            self.renderer.AddActor(actor)


    def pick_landmark(self, obj, event):
        x, y = obj.GetEventPosition()
        self.picker.Pick(x,y,0, self.renderer)
