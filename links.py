#!/usr/bin/python

import frames
import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
from IPython.display import display


class Link(frames.Frame):
    def __init__(self, lid, d, theta, r, alpha):
        self.lid = lid
        self._dh = (d, theta, r, alpha)
        t = "transl(.0,.0,%s)*rotz(%s)*transl(%s,.0,.0)*rotx(%s)"%(
          str(d),str(theta),str(r),str(alpha))
        super(Link, self).__init__(lid, transf=t)
  
    def _post_attach(self, parent):
        if isinstance(parent, Link):
            self._parseTransf()


class MultiLink(object):
    def __init__(self):
        self.tt = frames.TransformationTree()
        self.links = []
        self.eff = sp.Matrix([0,0,0,1]) # End effector
        self.H = None
        self.J = None
        self.H_eff = None
        self._boundSym = {}

    def compose(self, *kargs):
        self.H = self.tt.compose()
        self.H_eff = self.H*self.eff
        symbols = [frames.SYMB(s) for s in kargs]
        if symbols:
          self.J = self.H_eff.jacobian(symbols)

    def addLink(self, dhpars):
        p = self.tt.root
        if self.links: p=self.links[-1]
        l = Link("l%d"%len(self.links), *dhpars)
        l.parent = p
        self.links.append(l)
        return l

    def plotLinks(self, verbose=True, use3D=True):
      # First draw all the reference systems:
      ax = self.tt.plotFrames(Q=self._boundSym, use3D=use3D)

      # Now draw all the links
      for i,link in enumerate(self.links):
        # For each link, recalculate the old and the new origin
        O_a = self.tt.Hs[i].subs(self._boundSym) * frames.Oo
        O_b = self.tt.Hs[i+1].subs(self._boundSym) * frames.Oo
        # Now connect them
        if use3D:
          ax.plot( [O_b[0], O_a[0]], [O_b[1], O_a[1]], [O_b[2], O_a[2]], 
            'o-', lw=4, mew=5, alpha=0.7)
        else:
          ax.plot( [O_b[0], O_a[0]], [O_b[2], O_a[2]], 
            'o-', lw=4, mew=5, alpha=0.7)

    def bindSymbols(self, syms):
      self._boundSym = syms
      
    def getJacobian(self):
      return self.J.subs(self._boundSym)

    def jointsToPosition(self, *kargs, **kwargs):
      """ This is the direct kinematic """
      return self.H_eff.subs(self._boundSym)

    def isReachable(self, x,y,z):
      """ TODO: Tests if the point is inside the circle made by the arm"""
      return True
      
    def fromDH(self, pars):
      d = pars["d"]
      theta = pars["theta"]
      r = pars["r"]
      alpha = pars["alpha"]
      _p = zip(d,theta,r,alpha)
      for i, v in enumerate(_p):
        print self.addLink(v)
    





