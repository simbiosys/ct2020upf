import frames
import numpy as np


def _s(s):
  return frames.SYMB(s)

def unit_vector(vector):
  """ Returns the unit vector of the vector.  """

  vector = map(float, vector)
  return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
  """ Returns the angle in radians between vectors 'v1' and 'v2'::

          >>> angle_between((1, 0, 0), (0, 1, 0))
          1.5707963267948966
          >>> angle_between((1, 0, 0), (1, 0, 0))
          0.0
          >>> angle_between((1, 0, 0), (-1, 0, 0))
          3.141592653589793
  """
  v1_u = unit_vector(v1)
  v2_u = unit_vector(v2)
  return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def calculate_squared_distance(point1, point2):
  """ A function to calculate the Euclidean distance between two points """
  current_index = 0
  squared_distance = 0
  while current_index < len(point1) - 1 and current_index < len(point2) - 1:
      squared_distance += (point1[current_index] - point2[current_index]) ** 2
      current_index += 1
  return squared_distance

def project_onto_plane(x, n, md=3):
    d = x.dot(n) / n.norm()
    p = [d * n.normalized()[i] for i in range(len(n))]
    return [x[i] - p[i] if (i<md) else 1 for i in range(len(x)) ]